/* [Knob] */
knob_shape = "🔺 polygon"; // ["🔺 polygon","📁 file"]
// The SVG file to use if knob_shape="📁 file"
knob_shape_file = "";
// Edge count for knob_shape="🔺 polygon". 0 is a circle.
knob_edges = 6;       // [0:1:10]
knob_edge_radius = 1; // [0:0.1:10]
knob_diameter = 30;   // [5:0.1:100]
knob_thickness = 10;  // [1:0.1:30]

knob_text_show = true;
knob_text = "knob";
knob_text_depth = 1;         // [0.1:0.1:10]
knob_text_rotation = 0;      // [-180:1:180]
knob_text_offset = [ 0, 0 ]; // [-50:0.1:50]
knob_text_enlarge = 0;       // [0:0.01:1]
knob_text_shrink = 0;        // [0:0.01:1]
knob_text_size = 20;         // [1:0.1:100]

/* [Hole] */
hole_shape = "🔺 polygon"; // ["🔺 polygon","📁 file"]
// The SVG file to use if hole_shape="📁 file"
hole_shape_file = "";
// Edge count for hole_shape="🔺 polygon". 0 is a circle.
hole_edges = 3;       // [0:1:10]
hole_edge_radius = 1; // [0:0.1:10]
hole_diameter = 10;   // [5:0.1:100]
hole_depth = 5;       // [1:0.1:30]

/* [Precision] */
epsilon = 0.1 * 1;
$fs = $preview ? 2 : 0.5;
$fa = $preview ? 3 : 1;

module
knob_shape()
{
  offset(knob_edge_radius)
    offset(-knob_edge_radius) if (knob_shape == "🔺 polygon")
      circle(d = knob_diameter,
             $fn = knob_edges > 0
                     ? knob_edges
                     : max($fn, PI * knob_diameter / $fs, 360 / $fa));

  else if (knob_shape == "📁 file") import(knob_shape_file);
}

module
knob_body()
{
  linear_extrude(knob_thickness) knob_shape();
}

module
hole_shape()
{
  offset(hole_edge_radius)
    offset(-hole_edge_radius) if (hole_shape == "🔺 polygon")
      circle(d = hole_diameter,
             $fn = hole_edges > 0
                     ? hole_edges
                     : max($fn, PI * hole_diameter / $fs, 360 / $fa));

  else if (hole_shape == "📁 file") import(hole_shape_file);
}

module
hole_body()
{
  linear_extrude(hole_depth) hole_shape();
}

difference()
{
  knob_body();
  if (knob_text_show)
    translate([ 0, 0, knob_thickness - knob_text_depth + epsilon ])
      rotate([ 0, 0, knob_text_rotation ])

        linear_extrude(knob_text_depth) offset(knob_text_enlarge)
          offset(-knob_text_shrink) translate(knob_text_offset)
            resize([ knob_text_size, 0 ], auto = [ true, true ])
              text(knob_text, valign = "center", halign = "center");
  translate([ 0, 0, -epsilon ]) hole_body();
}
